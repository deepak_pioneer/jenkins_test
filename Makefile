OFLAG=-o
CFLAG=-c
CC=gcc

.PHONY: all
all: prog

prog: prog.c  add.o mean.o mul.o sub.o

clean:
	@rm -f prog
	@rm -f add.o
	@rm -f mean.o
	@rm -f mul.o
	@rm -f sub.o

